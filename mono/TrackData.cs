﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace CarBot
{
    public class DataContainer<T>
    {
        [JsonProperty("msgType")]
        public string MessageType { get; set; }

        public T Data { get; set; }

        public string GameId { get; set; }
    }

    public class GameData
    {
        public RaceData Race { get; set; }
    }

    public class RaceData
    {
        public TrackData Track { get; set; }

        public CarData[] Cars { get; set; }

        public RaceSessionData RaceSession { get; set; }
    }

    public class TrackData
    {
        public string Id { get; set; }

        public string Name { get; set; }

        public PieceData[] Pieces { get; set; }

        public LaneData[] Lanes { get; set; }

        public PointAndAngle StartingPoint { get; set; }

        public double TrackLength { get; set; }

        public void CalculateBestSpeed()
        {
            const double g = 0.33;
            const double straightSpeed = 50;
            int lanes = this.Lanes.Length;
            Bot.Log(string.Format("New race: {0}; Lanes: {1}", this.Name, lanes));

            double startCounter = 0;
            PieceData lastSwitchPiece = null;
            double[] timeSinceSwitch = new double[lanes];
            foreach (PieceData pd in Pieces)
            {
                pd.Lengths = new double[lanes];
                pd.MaxSpeeds = new double[lanes];
                pd.Times = new double[lanes];
                pd.PieceStart = startCounter;
                if (pd.Angle != 0)
                {
                    pd.Length = Math.Abs((2 * Math.PI * pd.Radius) * (pd.Angle / 360));
                    for (int i = 0; i < lanes; i++)
                    {
                        double r;
                        if (pd.Angle < 0)
                        {
                            r = pd.Radius - this.Lanes[i].DistanceFromCenter;
                        }
                        else
                        {
                            r = pd.Radius + this.Lanes[i].DistanceFromCenter;
                        }

                        double v = Math.Sqrt(g * r);

                        pd.Lengths[i] = pd.Length * (r / pd.Radius);
                        pd.MaxSpeeds[i] = v;
                        pd.Times[i] = pd.Lengths[i] / v;
                    }
                }
                else
                {
                    for (int i = 0; i < lanes; i++)
                    {
                        pd.Lengths[i] = pd.Length;
                        pd.MaxSpeeds[i] = straightSpeed;
                        pd.Times[i] = pd.Length / straightSpeed;
                    }
                }

                if (pd.IsSwitch)
                {
                    lastSwitchPiece = pd;
                    timeSinceSwitch = new double[lanes];
                }
                else
                {
                    for (int i = 0; i < lanes; i++)
                    {
                        timeSinceSwitch[i] += pd.Times[i];
                    }
                }

                startCounter += pd.Length;
            }
            
#if DEBUG
            int counter = 0;
#endif

            // Redo loop to set switch angles correctly, also logging
            foreach (PieceData pd in Pieces)
            {
                if (pd.IsSwitch)
                {
                    if (lastSwitchPiece != null)
                    {
                        Bot.Log(string.Format("In: {0:n2}; Out: {1:n2}", timeSinceSwitch[0], timeSinceSwitch[lanes - 1]));
                        if (timeSinceSwitch[0] < timeSinceSwitch[lanes - 1])
                        {
                            lastSwitchPiece.SwitchDirection = 1;
                        }
                        else if (timeSinceSwitch[0] > timeSinceSwitch[lanes - 1])
                        {
                            lastSwitchPiece.SwitchDirection = -1;
                        }
                    }

                    lastSwitchPiece = pd;
                    timeSinceSwitch = new double[lanes];
                }
                else
                {
                    for (int i = 0; i < lanes; i++)
                    {
                        timeSinceSwitch[i] += pd.Times[i];
                    }
                }

#if DEBUG
                var sb = new System.Text.StringBuilder();
                for (int i = 0; i < lanes; i++)
                {
                    sb.AppendFormat("\t[{0,5:n1}m/{1,5:n1}m/s = {2,4:n1}s]", pd.Lengths[i], pd.MaxSpeeds[i], pd.Times[i]);
                }

                Bot.Log(string.Format("Piece {4,2}:\t{0:000.0}\t{1,6:000.0}\t{2:000.0}\t{3}{5}", pd.Length, pd.Angle, pd.Radius, pd.IsSwitch, counter, sb.ToString()));
                counter++;
#endif
            }

            TrackLength = startCounter;
            Bot.Log(string.Format("Total:   \t{0:000.0}", startCounter));
        }
    }

    public class PieceData
    {
        public double Length { get; set; }

        [JsonProperty(PropertyName = "switch")]
        public bool IsSwitch { get; set; }

        public double Radius { get; set; }

        public double Angle { get; set; }

        [JsonIgnore]
        public double PieceStart { get; set; }

        [JsonIgnore]
        public int SwitchDirection { get; set; }

        [JsonIgnore]
        public double[] Lengths { get; set; }

        [JsonIgnore]
        public double[] MaxSpeeds { get; set; }

        [JsonIgnore]
        public double[] Times { get; set; }

        public override string ToString()
        {
            return string.Format("L: {0:n1}; A: {1:n1}; R: {2:n1}; {3}", Length, Angle, Radius, IsSwitch);
        }
    }

    public class LaneData
    {
        public double DistanceFromCenter { get; set; }

        public double Index { get; set; }

        public override string ToString()
        {
            return DistanceFromCenter.ToString("n1");
        }
    }

    public class PointAndAngle
    {
        public Point Position { get; set; }

        public double Angle { get; set; }

        public override string ToString()
        {
            return string.Format("{0}; Angle: {1:n2}", this.Position, this.Angle);
        }
    }

    public class Point
    {
        public double X { get; set; }

        public double Y { get; set; }

        public override string ToString()
        {
            return string.Format("X: {0:n2}; Y: {0:n2}", this.X, this.Y);
        }
    }

    public class CarData
    {
        public CarId Id { get; set; }

        public CarDimension Dimensions { get; set; }

        public override string ToString()
        {
            return this.Id.Name;
        }
    }

    public class CarId
    {
        public string Name { get; set; }

        public string Color { get; set; }
    }

    public class CarDimension
    {
        public double Length { get; set; }

        public double Width { get; set; }

        public double GuideFlagPosition { get; set; }
    }

    public class RaceSessionData
    {
        public int Laps { get; set; }

        [JsonProperty("maxLapTimeMs")]
        public int MaxLapTime { get; set; }

        public bool QuickRace { get; set; }
    }

    public class CarPosition
    {
        public CarId Id { get; set; }

        public double Angle { get; set; }

        public PiecePosition PiecePosition { get; set; }
    }

    public class PiecePosition
    {
        public int PieceIndex { get; set; }

        public double InPieceDistance { get; set; }

        public LanePosition Lane { get; set; }
    }

    public class LanePosition
    {
        public double StartLaneIndex { get; set; }

        public double StopLaneIndex { get; set; }
    }

    public static class MathHelper
    {
        public static int GetClosestAccelerationPolynomial(double value)
        {
            int counter = 0;
            double lowerValue;
            double upperValue = 0;
            do
            {
                counter++;
                lowerValue = upperValue;
                upperValue = (0.000003632192 * Math.Pow(counter, 3)) - (0.001355316022 * Math.Pow(counter, 2)) + (0.186718776394 * counter) - 0.18537;
            } while (upperValue < value);

            double diff1 = upperValue - value;
            double diff2 = lowerValue - value;
            if (diff1 < diff2)
            {
                return counter;
            }
            else
            {
                return counter - 1;
            }
        }

        public static int GetSlowSteps(double from, double to)
        {
            if (to > from)
            {
                return 0;
            }

            return (int)Math.Round((from - to) / 0.127699153226);
        }
    }
}