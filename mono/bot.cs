namespace CarBot
{
    using System;
    using System.IO;
    using System.Net.Sockets;
    using Newtonsoft.Json;

    public class Bot
    {
        private GameData gameData = null;
#if DEBUG
        private static VisualDebugger dbg;
#endif

        public static void Main(string[] args)
        {
            try
            {
                string host = args[0];
                int port = int.Parse(args[1]);
                string botName = args[2];
                string botKey = args[3];

                dbg = new VisualDebugger();
                dbg.Show();


                /*
                GameData gameData = JsonConvert.DeserializeObject<GameData>("{\"Race\":{\"Track\":{\"Id\":\"germany\",\"Name\":\"Germany\",\"Pieces\":[{\"Length\":100.0,\"switch\":false,\"Radius\":0.0,\"Angle\":0.0},{\"Length\":100.0,\"switch\":true,\"Radius\":0.0,\"Angle\":0.0},{\"Length\":39.269908169872416,\"switch\":false,\"Radius\":50.0,\"Angle\":45.0},{\"Length\":39.269908169872416,\"switch\":false,\"Radius\":50.0,\"Angle\":45.0},{\"Length\":39.269908169872416,\"switch\":false,\"Radius\":50.0,\"Angle\":45.0},{\"Length\":50.0,\"switch\":false,\"Radius\":0.0,\"Angle\":0.0},{\"Length\":39.269908169872416,\"switch\":false,\"Radius\":50.0,\"Angle\":-45.0},{\"Length\":39.269908169872416,\"switch\":false,\"Radius\":50.0,\"Angle\":-45.0},{\"Length\":39.269908169872416,\"switch\":false,\"Radius\":50.0,\"Angle\":-45.0},{\"Length\":50.0,\"switch\":false,\"Radius\":0.0,\"Angle\":0.0},{\"Length\":39.269908169872416,\"switch\":false,\"Radius\":50.0,\"Angle\":-45.0},{\"Length\":39.269908169872416,\"switch\":false,\"Radius\":50.0,\"Angle\":-45.0},{\"Length\":39.269908169872416,\"switch\":false,\"Radius\":100.0,\"Angle\":-22.5},{\"Length\":39.269908169872416,\"switch\":false,\"Radius\":50.0,\"Angle\":45.0},{\"Length\":100.0,\"switch\":true,\"Radius\":0.0,\"Angle\":0.0},{\"Length\":100.0,\"switch\":false,\"Radius\":0.0,\"Angle\":0.0},{\"Length\":39.269908169872416,\"switch\":false,\"Radius\":50.0,\"Angle\":-45.0},{\"Length\":50.0,\"switch\":false,\"Radius\":0.0,\"Angle\":0.0},{\"Length\":39.269908169872416,\"switch\":false,\"Radius\":50.0,\"Angle\":45.0},{\"Length\":39.269908169872416,\"switch\":false,\"Radius\":50.0,\"Angle\":45.0},{\"Length\":39.269908169872416,\"switch\":false,\"Radius\":100.0,\"Angle\":22.5},{\"Length\":100.0,\"switch\":true,\"Radius\":0.0,\"Angle\":0.0},{\"Length\":100.0,\"switch\":false,\"Radius\":0.0,\"Angle\":0.0},{\"Length\":39.269908169872416,\"switch\":false,\"Radius\":50.0,\"Angle\":45.0},{\"Length\":39.269908169872416,\"switch\":false,\"Radius\":50.0,\"Angle\":45.0},{\"Length\":39.269908169872416,\"switch\":false,\"Radius\":50.0,\"Angle\":45.0},{\"Length\":39.269908169872416,\"switch\":false,\"Radius\":50.0,\"Angle\":45.0},{\"Length\":100.0,\"switch\":true,\"Radius\":0.0,\"Angle\":0.0},{\"Length\":39.269908169872416,\"switch\":false,\"Radius\":50.0,\"Angle\":-45.0},{\"Length\":39.269908169872416,\"switch\":false,\"Radius\":50.0,\"Angle\":-45.0},{\"Length\":50.0,\"switch\":false,\"Radius\":0.0,\"Angle\":0.0},{\"Length\":39.269908169872416,\"switch\":false,\"Radius\":100.0,\"Angle\":22.5},{\"Length\":100.0,\"switch\":false,\"Radius\":0.0,\"Angle\":0.0},{\"Length\":50.0,\"switch\":false,\"Radius\":0.0,\"Angle\":0.0},{\"Length\":39.269908169872416,\"switch\":false,\"Radius\":100.0,\"Angle\":-22.5},{\"Length\":39.269908169872416,\"switch\":false,\"Radius\":100.0,\"Angle\":-22.5},{\"Length\":100.0,\"switch\":true,\"Radius\":0.0,\"Angle\":0.0},{\"Length\":39.269908169872416,\"switch\":false,\"Radius\":50.0,\"Angle\":45.0},{\"Length\":39.269908169872416,\"switch\":false,\"Radius\":50.0,\"Angle\":45.0},{\"Length\":39.269908169872416,\"switch\":false,\"Radius\":100.0,\"Angle\":22.5},{\"Length\":100.0,\"switch\":false,\"Radius\":0.0,\"Angle\":0.0},{\"Length\":100.0,\"switch\":false,\"Radius\":0.0,\"Angle\":0.0},{\"Length\":100.0,\"switch\":false,\"Radius\":0.0,\"Angle\":0.0},{\"Length\":78.539816339744831,\"switch\":false,\"Radius\":100.0,\"Angle\":45.0},{\"Length\":70.0,\"switch\":false,\"Radius\":0.0,\"Angle\":0.0},{\"Length\":100.0,\"switch\":true,\"Radius\":0.0,\"Angle\":0.0},{\"Length\":39.269908169872416,\"switch\":false,\"Radius\":50.0,\"Angle\":-45.0},{\"Length\":39.269908169872416,\"switch\":false,\"Radius\":50.0,\"Angle\":-45.0},{\"Length\":78.539816339744831,\"switch\":false,\"Radius\":100.0,\"Angle\":45.0},{\"Length\":78.539816339744831,\"switch\":false,\"Radius\":100.0,\"Angle\":45.0},{\"Length\":50.0,\"switch\":false,\"Radius\":0.0,\"Angle\":0.0},{\"Length\":78.539816339744831,\"switch\":false,\"Radius\":100.0,\"Angle\":45.0},{\"Length\":78.539816339744831,\"switch\":false,\"Radius\":100.0,\"Angle\":45.0},{\"Length\":78.539816339744831,\"switch\":true,\"Radius\":100.0,\"Angle\":45.0},{\"Length\":100.0,\"switch\":false,\"Radius\":0.0,\"Angle\":0.0},{\"Length\":100.0,\"switch\":false,\"Radius\":0.0,\"Angle\":0.0},{\"Length\":59.0,\"switch\":false,\"Radius\":0.0,\"Angle\":0.0}],\"Lanes\":[{\"DistanceFromCenter\":-10.0,\"Index\":0.0},{\"DistanceFromCenter\":10.0,\"Index\":1.0}],\"StartingPoint\":{\"Position\":{\"X\":-12.0,\"Y\":192.0},\"Angle\":-90.0},\"TrackLength\":3499.7963267948976},\"Cars\":[{\"Id\":{\"Name\":\"ttv86\",\"Color\":\"red\"},\"Dimensions\":{\"Length\":40.0,\"Width\":20.0,\"GuideFlagPosition\":10.0}}],\"RaceSession\":{\"Laps\":3,\"maxLapTimeMs\":60000,\"QuickRace\":true}}}");
                gameData.Race.Track.CalculateBestSpeed();
                dbg.SetTrack(gameData);
                //dbg.SetData(200, 300, 10);
                System.Windows.Forms.Application.Run(dbg);
                return;
                /* */

                Bot.Log("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

                using (TcpClient client = new TcpClient(host, port))
                {
                    NetworkStream stream = client.GetStream();
                    StreamReader reader = new StreamReader(stream);
                    StreamWriter writer = new StreamWriter(stream);
                    writer.AutoFlush = true;

                    Bot bot = new Bot(reader, writer);
#if DEBUG
                    bot.DoRace(new CreateRace(botName, botKey, "usa", null, 1));
#else
                    bot.DoRace(new Join(botName, botKey));
#endif
                }
            }
            catch (Exception exp)
            {
                Console.Write(exp.Message);
                Console.Write(exp.StackTrace);
            }
        }

        private StreamReader reader;
        private StreamWriter writer;

        private Bot(StreamReader reader, StreamWriter writer)
        {
            this.writer = writer;
            this.reader = reader;
        }

        internal void DoRace(SendMsg create)
        {
            string line;

            bool isCrashed = false;
            CarId myCarId = null;
            double lastPos = 0;
            PieceData[] pieces = null;
            int switchIndex = -1;
            SwitchLane nextSwitch = null;
            int lastIndex = 0;
            double lastSpeed = 0;

            Send(create);
            Bot.Log("Connected.");
            while ((line = reader.ReadLine()) != null)
            {
#if DEBUG
                //Log(line);
#endif
                MsgWrapper msg = JsonConvert.DeserializeObject<MsgWrapper>(line);
                switch (msg.msgType)
                {
                    case "carPositions":
                        if (!isCrashed)
                        {
                            CarPosition[] carPositions = ((Newtonsoft.Json.Linq.JToken)msg.data).ToObject<CarPosition[]>();
                            CarPosition myCarPosition = null;
                            if (myCarId != null)
                            {
                                foreach (CarPosition carPosition in carPositions)
                                {
                                    if (carPosition.Id.Color == myCarId.Color)
                                    {
                                        myCarPosition = carPosition;
                                    }
                                }
                            }
                            else
                            {
                                myCarPosition = carPositions[0];
                            }

                            int index = myCarPosition.PiecePosition.PieceIndex;
                            if (index == switchIndex)
                            {
                                switchIndex = -1;
                            }

                            double currentPos = pieces[index].PieceStart + myCarPosition.PiecePosition.InPieceDistance;
                            double speed = Math.Max(currentPos - lastPos, 0);
                            if (index != lastIndex)
                            {
                                speed = lastSpeed;
                                lastIndex = index;
                            }
                            else
                            {
                                lastSpeed = speed;
                            }

                            int skipCount = 5;
                            double lookAhead = Math.Max(speed, 1) * 10;
                            while ((skipCount > 0) && (myCarPosition.PiecePosition.InPieceDistance > gameData.Race.Track.Pieces[index].Length - lookAhead))
                            {
                                skipCount--;
                                index = (index + 1) % gameData.Race.Track.Pieces.Length;
                                if (pieces[index].IsSwitch && (switchIndex == -1))
                                {
                                    if (pieces[index].SwitchDirection < 0)
                                    {
                                        Log("Switch coming. Go left.");
                                        nextSwitch = new SwitchLane(true);
                                    }
                                    else if (pieces[index].SwitchDirection > 0)
                                    {
                                        Log("Switch coming. Go right.");
                                        nextSwitch = new SwitchLane(false);
                                    }
                                    else
                                    {
                                        Log("Switch coming. Do nothing.");
                                    }

                                    switchIndex = index;
                                }
                            }

                            if (nextSwitch != null)
                            {
                                Send(nextSwitch);
                                nextSwitch = null;
                            }
                            else
                            {
                                double angle = Math.Max(Math.Abs(pieces[myCarPosition.PiecePosition.PieceIndex].Angle), Math.Abs(pieces[index].Angle));
                                double maxSpeed = pieces[index].MaxSpeeds[(int)myCarPosition.PiecePosition.Lane.StartLaneIndex]; // 14 - ((angle / pieces[index].Length) * 15);
                                double throttle = speed < maxSpeed ? 1 : 0; // 1 - (angle / 70d);
                                throttle = Math.Max(Math.Min(throttle, 1), 0.1);
#if DEBUG
                                dbg.SetData(currentPos, currentPos + lookAhead, speed, maxSpeed, pieces[myCarPosition.PiecePosition.PieceIndex].Angle, myCarPosition.Angle);
                                Log(string.Format("{6:000}\t{0:000.00000}\t{1:00.0000}\t{2:n2}\t{5:n2}\t{3:n2}\t{4:n2}", currentPos, speed, angle, throttle, myCarPosition.Angle, maxSpeed, myCarPosition.PiecePosition.PieceIndex));
#endif
                                Send(new Throttle(throttle));
                            }

                            lastPos = currentPos;
                        }
                        else
                        {
                            Send(new Throttle(0));
                        }

                        break;
                    case "yourCar":
                        myCarId = ((Newtonsoft.Json.Linq.JToken)msg.data).ToObject<CarId>();
                        Log("Joined");
                        Send(new Ping());
                        break;
                    case "gameInit":
                        gameData = ((Newtonsoft.Json.Linq.JToken)msg.data).ToObject<GameData>();
                        gameData.Race.Track.CalculateBestSpeed();
#if DEBUG
                        dbg.SetTrack(gameData);
#endif
                        pieces = gameData.Race.Track.Pieces;
                        Log("Race init");
                        Log("Til\tPos     \tSpeed\tAngle\tMax\tThrotl.\tAngle");
                        Send(new Ping());
                        break;
                    case "gameEnd":
                        Log("Race ended");
                        Send(new Ping());
                        break;
                    case "gameStart":
                        Log("Race starts");
                        Send(new Ping());
                        break;
                    case "crash":
                        Log("Crash");
                        isCrashed = true;
                        Send(new Ping());
                        break;
                    case "spawn":
                        Log("Respawn");
                        isCrashed = false;
                        Send(new Ping());
                        break;
                    default:
                        Log(line);
                        Send(new Ping());
                        break;
                }
            }

#if DEBUG
            dbg.SaveData();
#endif
        }

        private static string lastLine = string.Empty;
        internal static void Log(string line)
        {
            if (line != lastLine)
            {
                Console.WriteLine(line);
#if DEBUG
                System.IO.File.AppendAllText(@"e:\temp\car.txt", DateTime.Now.ToString("s") + " " + line + Environment.NewLine);
#endif
                lastLine = line;
            }
        }

        private void Send(SendMsg msg)
        {
            string sendText = msg.ToJson();
            writer.WriteLine(sendText);
        }

        public GameData GameData { get { return this.gameData; } }
    }

    class MsgWrapper
    {
        public string msgType;
        public Object data;

        public MsgWrapper(string msgType, Object data)
        {
            this.msgType = msgType;
            this.data = data;
        }
    }

    abstract class SendMsg
    {
        public string ToJson()
        {
            return JsonConvert.SerializeObject(new MsgWrapper(this.MsgType(), this.MsgData()));
        }
        protected virtual Object MsgData()
        {
            return this;
        }

        protected abstract string MsgType();
    }

    class Join : SendMsg
    {
        public string name;
        public string key;
        public string color;

        public Join(string name, string key)
        {
            this.name = name;
            this.key = key;
            this.color = "red";
        }

        protected override string MsgType()
        {
            return "join";
        }
    }

    class CreateRace : SendMsg
    {
        public CreateRace(string name, string key, string trackName, string password, int carCount)
        {
            this.BotId = new CarKey() { Key = key, Name = name };
            this.TrackName = trackName;
            this.Password = password;
            this.CarCount = carCount;
        }

        [JsonProperty("botId")]
        public CarKey BotId { get; set; }

        [JsonProperty("trackName", NullValueHandling = NullValueHandling.Ignore)]
        public string TrackName { get; set; }

        [JsonProperty("password", NullValueHandling = NullValueHandling.Ignore)]
        public string Password { get; set; }

        [JsonProperty("carCount")]
        public int CarCount { get; set; }

        protected override string MsgType()
        {
            return "createRace";
        }
    }

    class JoinRace : CreateRace
    {
        public JoinRace(string name, string key, string trackName, string password, int carCount)
            : base(name, key, trackName, password, carCount)
        {
        }

        protected override string MsgType()
        {
            return "joinRace";
        }
    }

    class CarKey
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("key")]
        public string Key { get; set; }
    }

    class Ping : SendMsg
    {
        protected override string MsgType()
        {
            return "ping";
        }
    }

    class Throttle : SendMsg
    {
        public double value;

        public Throttle(double value)
        {
            this.value = value;
        }

        protected override Object MsgData()
        {
            return this.value;
        }

        protected override string MsgType()
        {
            return "throttle";
        }
    }

    class SwitchLane : SendMsg
    {
        public string value;

        public SwitchLane(bool left)
        {
            this.value = left ? "Left" : "Right";
        }

        protected override Object MsgData()
        {
            return this.value;
        }

        protected override string MsgType()
        {
            return "switchLane";
        }
    }
}